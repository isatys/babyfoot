let table = document.createElement('table');
let thead = document.createElement('thead');
let tbody = document.createElement('tbody');

table.appendChild(thead);
table.appendChild(tbody);

// Adding the entire table to the body tag
document.getElementById('body').appendChild(table);
// Creating and adding data to first row of the table
let row_1 = document.createElement('tr');
let heading_1 = document.createElement('th');
heading_1.innerHTML = "Classement";
let heading_2 = document.createElement('th');
heading_2.innerHTML = "Nom";
let heading_3 = document.createElement('th');
heading_3.innerHTML = "Classe";
let heading_4 = document.createElement('th');
heading_4.innerHTML = "Nombre de fois gagne";

row_1.appendChild(heading_1);
row_1.appendChild(heading_2);
row_1.appendChild(heading_3);
row_1.appendChild(heading_4);
thead.appendChild(row_1);


// Creating and adding data to second row of the table
let row_2 = document.createElement('tr');
let row_2_data_1 = document.createElement('td');
row_2_data_1.innerHTML = "1.";
let row_2_data_2 = document.createElement('td');
row_2_data_2.innerHTML = "Franck";
let row_2_data_3 = document.createElement('td');
row_2_data_3.innerHTML = "Master 1";
let row_2_data_4 = document.createElement('td');
row_2_data_4.innerHTML = "2";

row_2.appendChild(row_2_data_1);
row_2.appendChild(row_2_data_2);
row_2.appendChild(row_2_data_3);
row_2.appendChild(row_2_data_4);
tbody.appendChild(row_2);


// Creating and adding data to third row of the table
let row_3 = document.createElement('tr');
let row_3_data_1 = document.createElement('td');
row_3_data_1.innerHTML = "2.";
let row_3_data_2 = document.createElement('td');
row_3_data_2.innerHTML = "Zazou";
let row_3_data_3 = document.createElement('td');
row_3_data_3.innerHTML = "Master 1";
let row_3_data_4 = document.createElement('td');
row_3_data_4.innerHTML = "1000000";

row_3.appendChild(row_3_data_1);
row_3.appendChild(row_3_data_2);
row_3.appendChild(row_3_data_3);
row_3.appendChild(row_3_data_4);
tbody.appendChild(row_3);