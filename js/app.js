// show a message with a type of the input
function showMessage(input, message, type) {
	// get the small element and set the message
	const msg = input.parentNode.querySelector("small");
	msg.innerText = message;
	// update the class for the input
	input.className = type ? "success" : "error";
	return type;
}

function showError(input, message) {
	return showMessage(input, message, false);
}

function showSuccess(input) {
	return showMessage(input, "", true);
}

function hasValue(input, message) {
	if (input.value.trim() === "") {
		return showError(input, message);
	}
	return showSuccess(input);
}
function validatepromo(input, message) {
	if (input.value.trim() === "") {
		return showError(input, message);
	}
	return showSuccess(input);
}

function validatepoint(input, message) {
	if (input.value.trim() === "") {
		return showError(input, message);
	}
	return showSuccess(input);
}
const form = document.querySelector("#signup");

const NAME_REQUIRED = "Please enter your name";
const PROMO_REQUIRED = "Please enter your promo";
const POINT_REQUIRED = "Please enter your point";
const PROMO_INVALID = "Please enter a correct promo address format";

form.addEventListener("submit", function (event) {
	// stop form submission
	event.preventDefault();

	// validate the form
	let nameValid = hasValue(form.elements["name"], NAME_REQUIRED);
	let promoValid = validatepromo(form.elements["promo"], PROMO_REQUIRED, PROMO_INVALID);
	let pointValid = validatepoint(form.elements["point"],POINT_REQUIRED);
	// if valid, submit the form.
	if (nameValid && promoValid && pointValid) {
		alert("Vous etes bien enregistré");
	}
});



// Requesting permission for Notifications after clicking on the button

const button = document.getElementById('notifications');
button.addEventListener('click', () => {
  Notification.requestPermission().then((result) => {
    if (result === 'granted') {
      randomNotification();
    }
  });
});
// Setting up random Notification

function randomNotification() {
  const randomItem = Math.floor(Math.random() * formations.length);
  const notifTitle = formations[randomItem].name;
  const notifBody = `Win ${formations[randomItem].author}.`;
  const notifImg = `data/img/${formations[randomItem].slug}.jpg`;
  const options = {
    body: notifBody,
    icon: notifImg,
  };
  new Notification(notifTitle, options);
  setTimeout(randomNotification, 3000);
}
<<<<<<< HEAD
=======


// Au chargement de la page, déclencher la fonction ajaxBox_update()
window.onload = ajaxBox_update;

// Fonction effectuant la requête AJAX

function ajaxBox_update()
{
// Récupération de l'objet XHR
var xhr = getXhr();

// On assigne une fonction qui, lorsque l'état de la requête change, va traiter le résultat
xhr.onreadystatechange = function()
{
// readyState 4 = requête terminée
if (xhr.readyState == 4)
{
// status 200 = page requêtée trouvée
if (xhr.status == 200)
ajaxBox_parseJson(xhr.responseText); // Traitement JSON
// Page non trouvée
else
ajaxBox_setText('Error...');
}
};

// Préparation de la requête
var url = 'http://site.com/file.json';
xhr.open('GET', url, true);

// Execution de la requête
xhr.send(null);
}

// Fonction de traitement des données JSON retournées
// Récupère le noeud JSON data.texte_1

function ajaxBox_parseJson(pJsonString)
{
var jsonObjet = eval('('+ pJsonString +')');
ajaxBox_setText(jsonObjet.data.text_1);
}


// Fonction de mise à jour du contenu de la div #ajaxBox
// Ajout d'un element <p> contenant le message, dans le div #ajaxBox
function ajaxBox_setText(pText)
{
var elt = document.getElementById('ajaxBox');
var p = elt.appendChild(document.createElement('p'));
p.appendChild(document.createTextNode(pText));
}



let content = '';
for (let i = 0; i < formations.length; i++) {
  let entry = template.replace(/POS/g, (i + 1))
    
    .replace(/NAME/g, formations[i].name)
    .replace(/SECTEUR/g, formations[i].secteur)
    .replace(/WEBSITE/g, formations[i].website)
    
  entry = entry.replace('<a href=\'http:///\'></a>', '-');
  content += entry;
}
document.getElementById('body').innerHTML = content;
>>>>>>> c2642f9e469251fb6486cb75aba6be67d515a6a9
