self.addEventListener('push', function(event) {
    const payload = event.data ? event.data.text() : 'no payload';
    event.waitUntil(
    self.registration.showNotification('ServiceWorker Cookbook', {
    body: payload,
    })
    );
   });
   



//    self.importScripts('js/file.json');

// // Files to cache
// const cacheName = 'js13kPWA-v1';
// const appShellFiles = [
//   '/pwa-examples/js13kpwa/',
//   '/pwa-examples/js13kpwa/index.html',
//   '/pwa-examples/js13kpwa/app.js',
//   '/pwa-examples/js13kpwa/style.css',
  
// ];


// // Installing Service Worker
// self.addEventListener('install', (e) => {
//   console.log('[Service Worker] Install');
//   e.waitUntil((async () => {
//     const cache = await caches.open(cacheName);
//     console.log('[Service Worker] Caching all: app shell and content');
//     await cache.addAll(contentToCache);
//   })());
// });

// // Fetching content using Service Worker
// self.addEventListener('fetch', (e) => {
//   e.respondWith((async () => {
//     const r = await caches.match(e.request);
//     console.log(`[Service Worker] Fetching resource: ${e.request.url}`);
//     if (r) return r;
//     const response = await fetch(e.request);
//     const cache = await caches.open(cacheName);
//     console.log(`[Service Worker] Caching new resource: ${e.request.url}`);
//     cache.put(e.request, response.clone());
//     return response;
//   })());
// });
