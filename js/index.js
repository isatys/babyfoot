navigator.serviceWorker.register('serviceworker.js')
.then(function(registration) {
 return registration.pushManager.getSubscription()
 .then(async function(registration) {
 // partie relative à l'enregistrement
 });
})
.then(function(subscription) {
 // partie relative à l'abonnement
});

if(registration) {
    return registration;
   }
   const response = await fetch('./vapidPublicKey');
   const vapidPublicKey = await response.text();
   const convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);

   return registration.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: convertedVapidKey
   });

   fetch('./register', {
    method: 'post',
    headers: {
    'Content-type': 'application/json'
    },
    body: JSON.stringify({
    subscription: subscription
    }),
   });
   

   document.getElementById('doIt').onclick = function() {
    const payload = document.getElementById('notification-payload').value;
    const delay = document.getElementById('notification-delay').value;
    const ttl = document.getElementById('notification-ttl').value;
    fetch('./sendNotification', {
    method: 'post',
    headers: {
    'Content-type': 'application/json'
    },
    body: JSON.stringify({
    subscription: subscription,
    payload: payload,
    delay: delay,
    ttl: ttl,
    }),
    });
   };